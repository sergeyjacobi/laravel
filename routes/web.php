<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//})->name('welcome');

Auth::routes();

Route::get('login/github', 'Auth\GithubController@redirectToProvider')->name('github.login');
Route::get('login/github/callback', 'Auth\GithubController@handleProviderCallback');

//Route::match(['get', 'post'], '/create', 'PasteController@create')->name('create');
Route::match(['get', 'post'], '/', 'PasteController@create')->name('create');
Route::get('/private', 'PasteController@private')->name('private')->middleware('auth');
Route::get('/{hash}', 'PasteController@show')->where('hash', '[A-Za-z0-9]{8}')->name('hash');