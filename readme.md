# Инструкция по развертыванию проекта

* склонировать репозиторий командой "git clone git@bitbucket.org:sergeyjacobi/laravel.git"
* создать базу данных MySQL с кодировкой utf8mb4 и сортировкой utf8mb4_unicode_ci
* переименовать файл настроек .env.example на .env, настроить конфигурационный файл .env для своего подключения к базе данных и github аккаунту
* точка входа в приложение app/public/index.php
* используемая версия PHP 7.2
* после вышеуказанных настроек нужно применить миграции находясь в корневой директории проекта папка app, выполнить команду "php artisan migrate"
* в системный крон bash "crontab -e" добавить новую задачу с указанием пути к проекту "\* \* \* \* \* php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1"
* проект готов к работе

## Новые переменные для файла настроек .env
* GITHUB_CLIENT_ID=
* GITHUB_CLIENT_SECRET=
* GITHUB_CALLBACK_URL=http://sergeyjacobi.ml/login/github/callback
