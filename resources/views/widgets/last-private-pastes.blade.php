<div class="panel panel-danger">
    <div class="panel-heading">Your Last Pastes</div>
    <div class="panel-body">

        @if (!$pastes->isEmpty())
            <ul class="list-group ">

                @foreach ($pastes as $paste)
                    <li class="list-group-item">
                        Paste Name: <a href="{{ route('hash', $paste->hash) }}">{{ $paste->name }}</a><br>
                        Paste Expiration: {{ $paste->pasteExpiration() }}<br>
                        Paste Access: {{ $form_params['access_type'][$paste->access_type] }}
                    </li>
                @endforeach

            </ul>
        @else
            <p>Not Found</p>
        @endif
    </div>
</div>