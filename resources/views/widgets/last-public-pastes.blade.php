<div class="panel panel-default">
    <div class="panel-heading">Last Public Pastes</div>
    <div class="panel-body">

        @if (!$pastes->isEmpty())
            <ul class="list-group ">

                @foreach ($pastes as $paste)
                    <li class="list-group-item">
                        Paste Name: <a href="{{ route('hash', $paste->hash) }}">{{ $paste->name }}</a><br>
                        Paste Expiration: {{ $paste->pasteExpiration() }}
                    </li>
                @endforeach

            </ul>
        @else
            <p>Not Found</p>
        @endif
    </div>
</div>