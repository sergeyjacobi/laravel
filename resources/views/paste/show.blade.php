@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Paste name - {{ $model->name }}</div>

        <div class="panel-body">

            <pre><code style="" class="{{ $form_params['syntax'][$model->syntax] }}">{{ $model->text }}</code></pre>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">Syntax: {{ $form_params['syntax'][$model->syntax] }}</li>
                <li class="list-group-item">Paste Expiration: {{ $model->pasteExpiration() }}</li>
                <li class="list-group-item">Paste Access: {{ $form_params['access_type'][$model->access_type] }}</li>
            </ul>

        </div>
    </div>

@endsection
