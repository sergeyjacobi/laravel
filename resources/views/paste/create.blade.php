@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">New Paste</div>

        <div class="panel-body">
            {{ Form::open(array('action' => 'PasteController@create')) }}

            <div class="form-group{{ $errors->has('text') ? ' has-error' : '' }}">

                <label for="text" class="col control-label">Text</label>

                <div class="col">
                    <textarea id="text" class="form-control" name="text" rows="10" id="text" autofocus>{{ old('text') }}</textarea>

                    @if ($errors->has('text'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col control-label">Paste name</label>

                <div class="col">

                    {{ Form::input('text','name', old('name'), ['class' => 'form-control', 'id' => 'name', 'autocomplete' => 'off'] ) }}

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('syntax') ? ' has-error' : '' }}">
                <label for="syntax" class="col control-label">Syntax Highlighting</label>

                <div class="col">

                    {{ Form::select('syntax', $form_params['syntax'], old('syntax'), ['class' => 'form-control', 'id' => 'syntax'] ) }}

                    @if ($errors->has('syntax'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('syntax') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('expiration_type') ? ' has-error' : '' }}">
                <label for="expiration_type" class="col control-label">Paste Expiration</label>

                <div class="col">

                    {{ Form::select('expiration_type', $form_params['expiration_type'], old('expiration_type'), ['class' => 'form-control', 'id' => 'expiration_type'] ) }}

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('expiration_type') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('access_type') ? ' has-error' : '' }}">
                <label for="access_type" class="col control-label">Paste Access</label>

                <div class="col">

                    {{ Form::select('access_type', $form_params['access_type'], old('access_type'), ['class' => 'form-control', 'id' => 'access_type'] ) }}

                    @if ($errors->has('access_type'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('access_type') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col">
                    <button type="submit" class="btn btn-primary">
                        Create New Paste
                    </button>

                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
