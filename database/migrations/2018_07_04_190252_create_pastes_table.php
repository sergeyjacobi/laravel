<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePastesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pastes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,true)->nullable();
            $table->integer('expiration_type');
            $table->timestamp('expiration_time')->nullable();
            $table->tinyInteger('access_type');
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('syntax');
            $table->text('text');
            $table->char('hash',8);
            $table->string('name',150);
            $table->timestamps();

            //fk
            $table->index('access_type');
            $table->index('active');
            $table->index('hash');
            $table->unique('hash');
            $table->index('name');
            $table->index('syntax');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        //ft
        DB::statement('ALTER TABLE pastes ADD FULLTEXT pastes_text_ftindex (text)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pastes');
    }
}
