<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot ()
    {
        View::composer('widgets.last-public-pastes', 'App\Http\ViewComposers\LastPublicPastesComposer');
        View::composer('widgets.last-private-pastes', 'App\Http\ViewComposers\LastPrivatePastesComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register ()
    {
    }
}
