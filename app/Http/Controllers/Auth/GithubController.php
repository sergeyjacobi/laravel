<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

//use Socialite;

class GithubController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider ()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback ()
    {
        $user = Socialite::driver('github')->user();

        $auth_user = User::firstOrNew(['provider_id' => $user->id]);
        $auth_user->name = $user->nickname != null ? $user->nickname : $user->name;
        $auth_user->email = $user->email;
        $auth_user->provider = 'github';
        $auth_user->provider_id = $user->id;
        $auth_user->save();

        Auth::login($auth_user, true);

        return redirect('/');

    }
}