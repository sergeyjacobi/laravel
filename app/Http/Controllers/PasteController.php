<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paste;
use Dotenv\Validator;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;


class PasteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct ()
    {
        //$this->middleware('auth');
    }


    public function private ()
    {
        $form_params = Paste::formParams();

        return view('paste.private', [
            'pastes' => Paste::where(['active' => 1, 'user_id' => Auth::id()])->orderBy('id', 'desc')->paginate(10),
            'form_params' => $form_params
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create (Request $request)
    {
        $form_params = Paste::formParams();

        if ($request->isMethod('post')) {

            $request->validate([
                'text'            => 'required|min:5',
                'name'            => 'required|max:255|min:4',
                'expiration_type' => ['required', Rule::in(array_keys($form_params['expiration_type']))],
                'access_type'     => ['required', Rule::in(array_keys($form_params['access_type']))],
                'syntax'          => ['required', Rule::in(array_keys($form_params['syntax']))],
            ]);

            $model = new Paste();
            $model->fill($request->all());

            DB::transaction(function () use ($request,$model) {

                $model->save();

            }, 5);

            return redirect()->route('hash', ['hash' => $model->hash]);
        }

        return view('paste.create', [
            'form_params' => $form_params,
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Paste $paste
     *
     * @return \Illuminate\Http\Response
     */
    public function show ($hash)
    {
        $form_params = Paste::formParams();

        $model = Paste::where(['hash' => $hash, 'active' => 1])->first();

        $check = true;
        if ($model) {
            //access_type 3 - private
            if ($model->access_type == 3 && !Auth::check()) {
                $check = false;
            }
            if ($model->access_type == 3 && $model->user_id != Auth::id()) {
                $check = false;
            }
        } else {
            $check = false;
        }

        if ($check == false) {
            abort(404);
        }

        return view('paste.show', [
            'model'       => $model,
            'form_params' => $form_params,
        ]);


    }

}
