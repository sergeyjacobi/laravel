<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Users\Repository as UserRepository;
use App\Paste;
use Symfony\Component\VarDumper\VarDumper;

class LastPublicPastesComposer {

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $pastes = Paste::where(['active' => 1, 'access_type' => 1])->orderBy('id', 'desc')->limit(10)->get();

        $view->with('pastes', $pastes);
    }

}