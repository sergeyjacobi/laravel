<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Users\Repository as UserRepository;
use App\Paste;

class LastPrivatePastesComposer {

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $pastes = Paste::where(['active' => 1, 'user_id' => Auth::id()])->orderBy('id', 'desc')->limit(10)->get();
        $form_params = Paste::formParams();

        $view->with([
            'pastes' => $pastes,
            'form_params' => $form_params
        ]);
    }

}