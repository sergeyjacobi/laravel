<?php

namespace App\Components;

class Countdown
{

    public static function getTime ($date)
    {
        $timestamp = strtotime($date);
        $current_timestamp = time();

        $time_diff = $timestamp - $current_timestamp;

        //return $time_diff;

        if ($time_diff <= 0) {
            return 0;
        } else {
            $time_string = '';

            $weeks = 60 * 60 * 24 * 7;
            $days = 60 * 60 * 24;
            $hours = 60 * 60;
            $minutes = 60;

            //hide minutes and seconds
            $show_hours = true;
            $show_minutes = true;
            $show_seconds = true;

            if (($time_diff / $weeks) >= 1) {
                $t = floor($time_diff / $weeks);
                $time_string .= $t . ' weeks ';
                $time_diff -= $t * $weeks;

                $show_seconds = false;
                $show_minutes = false;
            }

            if (($time_diff / $days) >= 1) {
                $t = floor($time_diff / $days);
                $time_string .= $t . ' days ';
                $time_diff -= $t * $days;

                $show_seconds = false;
                $show_minutes = false;
            }

            if ($show_hours) {
                if (($time_diff / $hours) > 1) {
                    $t = floor($time_diff / $hours);
                    $time_string .= $t . ' hours ';
                    $time_diff -= $t * $hours;

                    $show_seconds = false;
                }
            }


            if ($show_minutes) {
                if (($time_diff / $minutes) >= 1) {
                    $t = floor($time_diff / $minutes);
                    $time_string .= $t . ' min ';
                    $time_diff -= $t * $minutes;
                }
            }


            if ($show_seconds) {
                if ($time_diff >= 1) {
                    $time_string .= $time_diff . ' sec ';
                }
            }


        }

        return $time_string;
    }
}