<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Paste;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [//
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule (Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {

            $current_date = date('Y-m-d H:i:s');

            $pastes = Paste::where(['active' => 1, ['expiration_type', '!=', 999],['expiration_time','<', date($current_date)]])->get();

            if (!$pastes->isEmpty()) {
                foreach ($pastes as $paste) {
                    $paste->active = 0;
                    $paste->save();
                }
            }

        })->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands ()
    {
        require base_path('routes/console.php');
    }
}
